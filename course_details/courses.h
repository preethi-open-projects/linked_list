#ifndef COURSES_H_
#define COURSES_H_

#include<stdint.h>
#include<stdbool.h>

#define MAX_LEN_BYTES 200

typedef struct __course_details{
		uint8_t course_name[MAX_LEN_BYTES];
		uint16_t credit_hours;
		uint16_t number_of_students;
		struct __course_details *next;
} course_details_t;

bool course_name_push(course_details_t *course_name);
course_details_t* course_pop(void);
course_details_t* course_peek(void);
bool push_to_front(course_details_t *course_name);
bool insert_new_course(course_details_t *course_name,uint16_t position);
uint16_t get_total_courses(void);
course_details_t* pop_by_position(uint16_t position);

void print_course_details(void);


#endif /* COURSES_H_ */
