#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include "courses.h"

static void _add_test_data(void);
static void _clear_data(void);

int main(void)
{
	uint16_t number_of_courses=0;
	course_details_t *temp;


	printf("Adding test details\n");
	_add_test_data();
	print_course_details();

	number_of_courses=get_total_courses();
	printf("TOtal courses : %d\r\n",number_of_courses);

	temp=pop_by_position(2);
	if(temp==NULL)
		printf("Failed to remove student from position\r\n");
	else
	{
		printf("Successfully removed student from position\r\n");
		free(temp);
		temp=NULL;
	}
	print_course_details();


	printf("Clearing test data\n");
	_clear_data();
	print_course_details();
	return 0;
}

static void _add_test_data(void)
{
	bool result;

	course_details_t *course1;
	course1=(course_details_t *)calloc(1,sizeof(course1));
	course1->credit_hours=3;
	course1->number_of_students=50;
	memcpy(course1->course_name,"Physics",7);
	result=course_name_push(course1);
	if(result==true)
		printf("Course added to list succesfully\n");
	else
		printf("Failed to add course to the list\n");

	course_details_t *course2;
	course2=(course_details_t *)calloc(1,sizeof(course2));
	course2->credit_hours=6;
	course2->number_of_students=100;
	memcpy(course2->course_name,"Mathematics",11);
	result=push_to_front(course2);
	if(result==true)
		printf("Course added to list succesfully\n");
	else
		printf("Failed to add course to the list\n");

	course_details_t *course3;
	course3=(course_details_t *)calloc(1,sizeof(course3));
	course3->credit_hours=2;
	course3->number_of_students=80;
	memcpy(course3->course_name,"Chemistry",9);
	result=insert_new_course(course3, 2);

}


static void _clear_data(void)
{
	course_details_t* temp;

	temp=course_peek();

	while(temp!=NULL)
	{
		course_pop();
		free(temp);
		temp=course_peek();
	}
}
