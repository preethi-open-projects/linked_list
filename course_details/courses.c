#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include "courses.h"

static course_details_t *_head;
static course_details_t *_tail;
static uint16_t _number_of_courses;
static course_details_t* _get_element_by_position(uint16_t);
////////////////////////////////////////////////////////////////
bool course_name_push(course_details_t *course_name)
{
	if(course_name==NULL)
		return false;

	if(_head==NULL)
	{
		_head=course_name;
	}

	else
	{
		_tail->next=course_name;
	}

	_tail=course_name;
	course_name->next=NULL;
	_number_of_courses++;

	return true;
}

bool push_to_front(course_details_t *course_name)
{

	if(course_name==NULL)
		return false;


	if(_head==NULL)
	{
		_head=NULL;
		_tail=NULL;
		course_name->next=NULL;
	}

	else
	{
		course_name->next=_head;
		_head=course_name;
		_number_of_courses++;
	}

	return true;
}

course_details_t* course_pop(void)
{
	course_details_t *first;

	if(_head==NULL)
		return NULL;

	first=_head;
	_head=_head->next;
	_number_of_courses--;

	if(_head==NULL)
		_tail=NULL;

	first->next=NULL;
	return first;

}

course_details_t* pop_by_position(uint16_t position)
{
	course_details_t *temp;
	course_details_t *prev;

	if(position==0)
		return NULL;

	if(position==1)
		return course_pop();

	if(position > _number_of_courses)
		return NULL;

	prev=_get_element_by_position(position-1);
	if(prev==NULL)
		return NULL;

	temp=prev->next;
	prev->next=temp->next;
	temp->next=NULL;

	if(position==_number_of_courses)
		_tail=prev;

	return temp;
}

course_details_t* course_peek(void)
{
	return _head;
}

uint16_t get_total_courses(void)
{
	return _number_of_courses;
}

bool insert_new_course(course_details_t *course_name,uint16_t position)
{
	course_details_t *prev;
	if(course_name==NULL)
		return NULL;

	prev=_get_element_by_position(position-1);
	if(prev==NULL)
		return course_name_push(course_name);

	course_name->next=prev->next;
	prev->next=course_name;
	_number_of_courses++;

	return false;


}

void print_course_details(void)
{
	course_details_t *temp;
	uint16_t number_of_courses=0;
	temp = _head;

	while(temp!=NULL)
	{
		printf("%s,%d,%d\r\n",temp->course_name,temp->credit_hours,
				temp->number_of_students);
		number_of_courses++;
		temp=temp->next;
	}

	printf("The total number of courses added are %d\n",number_of_courses);
}



/////////////////////////////////////////////////////////////////
static course_details_t* _get_element_by_position(uint16_t position)
{
	course_details_t *temp;
	temp=_head;
	uint16_t pos=1;

	if(position==1)
		return _head;


	while(temp!=NULL)
	{
		if(pos==position)
			return temp;

		pos++;
		temp=temp->next;
	}
	return NULL;
}
