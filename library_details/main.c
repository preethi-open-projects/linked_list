#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include"library.h"

static void _add_test_data(void);
static void _clear_data(void);

int main(void)
{
	uint16_t total_books=0;

	print_book_details();

	add_test_data();
	print_book_details();

	total_books = get_total_books();
	printf("The total number of books is %d\r\n",total_books);

	clear_data();
	print_book_details();
	return 0;
}

static void _add_test_data(void)
{
	bool result;

	book_details_t* book1;
	book1 = (book_details_t*)calloc(1,sizeof (book_details_t));
	book1->book_number=1;
		memcpy(book1->name,"Secret",6);
		result=books_push(book1);
		if(result==true)
		{
			printf("Sucessfully added book1 to the list\r\n");
		}
		else
		{
			printf("Failed to add book1 to list\r\n");
		}

		book_details_t* book2;
			book2 = (book_details_t*)calloc(1,sizeof (book_details_t));
			book2->book_number=2;
				memcpy(book2->name,"Suppandi",8);
				result=books_push(book2);
				if(result==true)
				{
					printf("Sucessfully added book2 to the list\r\n");
				}
				else
				{
					printf("Failed to add book2 to list\r\n");
				}

}

static void _clear_data(void)
{
	book_details_t *temp;
	temp=books_peek();

	while(temp!=NULL)
	{
		books_pop();
		free(temp);
		temp=books_peek();
	}
}
