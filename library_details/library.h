#ifndef LIBRARY_H_
#define LIBRARY_H_

#include<stdint.h>
#include<stdbool.h>

#define NAME_MAX_LEN_BYTES 200

typedef struct __book_details_{
	uint16_t book_number;
	uint8_t name[NAME_MAX_LEN_BYTES];
	struct  __book_details_ *next;
} book_details_t;

bool books_push(book_details_t *new_book);
book_details_t* books_pop(void);
book_details_t* books_peek(void);

uint16_t get_total_books(void);

void print_book_details(void);


#endif /* LIBRARY_H_ */
