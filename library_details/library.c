#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include"library.h"

static book_details_t *_head;
static book_details_t *_tail;
static uint16_t total_number_of_books;

///////////////////////////////////////////////////////////////public functions
bool books_push(book_details_t *new_book){

	if(new_book==NULL)
		return false;

	if(_head==NULL)
		_head=new_book;

	else
		_tail->next=new_book;

	_tail=new_book;
	new_book->next=NULL;

	total_number_of_books++;
	return true;

}
book_details_t* books_pop(void){
	book_details_t *first;

	if(_head==NULL)
	{
		return NULL;
	}
	else
	{
		first=_head;
		_head=_head->next;
		total_number_of_books--;
		if(_head==NULL)
			_tail=NULL;

		first->next=NULL;
		return first;
	}
	return NULL;
}

book_details_t* books_peek(void)
{
	return _head;
}

uint16_t get_total_books(void)
{
	return total_number_of_books;
}

///////////////////////////////////////////////////////////////////////////////Private functions

void print_book_details(void)
{
	book_details_t *temp;
	uint16_t _total_number_of_books;
	temp = _head;
	_total_number_of_books=0;

	while(temp!=NULL)
	{
		printf("%6d,%s\r\n",temp->book_number,temp->name);
		_total_number_of_books++;
		temp=temp->next;
	}

	printf("The number of books is %d\n",_total_number_of_books);


}


