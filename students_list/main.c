#include<stdio.h>
#include<stdint.h>
#include "students_list.h"
#include<stdbool.h>
#include<string.h>

#define MAX_STUDENTS 10

static student_info_t _student_info[MAX_STUDENTS];
static student_list_t _ece_students_pool;
static student_list_t _ece_students;

static uint8_t _roll_number;

static void _initialize_lists(void);
static void _print_list_size(void);
static void _add_students(void);
static void _clear_list(void);
////////////////////////////////////////////Public functions
int main(void)
{
	_initialize_lists();

	_roll_number=10;
	_add_students();
	_add_students();

	_roll_number=15;
	_add_students();
	_add_students();

	_print_list_size();
	student_print_details(&_ece_students);

	_clear_list();
	_print_list_size();

	return 0;
}

///////////////////////////////////////////Private functions
static void _add_students(void)
{
	/*
	 * Logic
	 * ========
	 * 1)Take empty structure from the pool.
	 * 2)Fill the empty structure with data.
	 * 3)Push the filled structure in the list.
	 */

	student_info_t *temp;
	bool result;

	temp=student_pop(&_ece_students_pool);
	if(temp==NULL)
	{
		printf("No buffer available in pool.Failed to add student\r\n");
		return;
	}
	temp->id=_roll_number;
	strcpy((char *)temp->name,"test");
	_roll_number++;

	result=student_push(&_ece_students, temp);
	if(result==false)
	{
		printf("Failed to add student to the list\r\n");
		result=student_push(&_ece_students_pool, temp);
		if(result==false)
			printf("Failed to add the struct back in pool"
					".There will be loss of structures.\n");
	}
}

static void _initialize_lists(void)
{
	student_init(&_ece_students_pool);
	student_init(&_ece_students);

	for(uint8_t i=0;i<MAX_STUDENTS;i++)
	{
		student_push(&_ece_students_pool, &_student_info[i]);
	}
	_print_list_size();
}

static void _print_list_size(void)
{
	printf("Pool size :%2d student size : %2d\r\n",student_get_size(&_ece_students_pool)
			,student_get_size(&_ece_students));
}

static void _clear_list(void)
{
	student_info_t *temp;

	temp=student_pop(&_ece_students);

	while(temp!=NULL)
	{
		student_push(&_ece_students_pool, temp);
		temp=student_pop(&_ece_students);
	}
}
