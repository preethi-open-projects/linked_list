#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include<stdlib.h>
#include "students_list.h"
#include "utils/linked_list.h"

//static linked_list_t _list;
static void _print_item_details(void* parent);
/////////////////////////////////////////////////Public Functions
void student_init(student_list_t *slist)
{
	linked_list_init(&slist->list);
}



bool student_push(student_list_t *slist,student_info_t *student)
{
	if(!student || !slist)
		return false;

	student->item.parent = (void *)student;

	return linked_list_push(&slist->list, &student->item);

}



bool student_push_to_front(student_list_t *slist,student_info_t *student)
{
	if(!student || !slist)
		return false;

	student->item.parent = (void *)student;

	return linked_list_push_to_front(&slist->list, &student->item);

}



student_info_t* student_pop(student_list_t *slist)
{
	linked_list_item_t* item;

	item = linked_list_pop(&slist->list);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}



student_info_t* student_pop_by_position(student_list_t *slist,uint16_t position)
{
	linked_list_item_t* item;

	item=linked_list_pop_by_position(&slist->list, position);
	if(item==NULL)
		return NULL ;

	return(student_info_t*)item->parent;
}



bool student_insert(student_list_t *slist,student_info_t *student,uint16_t position)
{
	if(!student || !slist)
		return false;

	student->item.parent=(void *)student;

	return linked_list_insert(&slist->list, &student->item, position);

}


student_info_t* student_peek(student_list_t *slist)
{
	if(!slist)
		return NULL;

	linked_list_item_t* item;

	item = linked_list_peek(&slist->list);
	if(item == NULL)
		return NULL;

	return(student_info_t*)item->parent;
}



uint16_t student_get_size(student_list_t *slist)
{
	if(!slist)
		return 0;

	return slist->list.size;
}



void student_print_details(student_list_t *slist)
{
	if(!slist)
		return;

	linked_list_print_details(&slist->list, _print_item_details);
}
///////////////////////////////////////////////////////Private functions
static void _print_item_details(void* parent)
{
	student_info_t* temp;

	if(!parent)
		return;

	temp = (student_info_t*) parent;
	printf("%4d, %s\r\n",temp->id,temp->name);
}

