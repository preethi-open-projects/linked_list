
#ifndef STUDENTS_LIST_H_
#define STUDENTS_LIST_H_

#define STUDENT_NAME_MAX_SIZE_BYTES 100

#include "utils/linked_list.h"
#include<stdint.h>

typedef struct _student_info {
	uint16_t id;
	uint8_t name[STUDENT_NAME_MAX_SIZE_BYTES];
	linked_list_item_t item;
}student_info_t;

typedef struct _student_list {
	linked_list_t list;
}student_list_t;

void student_init(student_list_t *slist);
bool student_push(student_list_t *slist,student_info_t *student);
student_info_t* student_pop(student_list_t *slist);
student_info_t* student_peek(student_list_t *slist);
bool student_push_to_front(student_list_t *slist,student_info_t *student);
bool student_insert(student_list_t *slist,student_info_t *student,uint16_t position);
student_info_t* student_pop_by_position(student_list_t *slist,uint16_t position);
uint16_t student_get_size(student_list_t *slist);
void student_print_details(student_list_t *slist);

#endif /* STUDENTS_LIST_H_ */
