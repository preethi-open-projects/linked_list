#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include "students.h"
#include"courses.h"
#include"utils/linked_list.h"

static void _add_test_data(void);
static void _clear_data(void);
static void _add_course_test_data(void);
static void _clear_course_data(linked_list_t *list);
static void _print_course_test_details(linked_list_t *list);

static linked_list_t ece_sem1;
static linked_list_t ece_sem2;


int main(void)
{
	uint16_t number_of_students=0;
	student_info_t *temp;
	uint16_t pos=2;

	print_student_details();

	_print_course_test_details(&ece_sem1);
	_print_course_test_details(&ece_sem2);

/////////////////////////////////////////Student module test case
	printf("Adding test data\r\n");
	_add_test_data();
	print_student_details();

	number_of_students=students_get_total();
	printf("The total number of students is %d\r\n",number_of_students);

	//position=get_student_position(roll_number);
	printf("The position of roll number %d is %d\r\n",10,get_student_position(10));

	printf("Removing student from position %d\n",pos);
	temp=students_pop_by_position(pos);
	if(temp==NULL)
		printf("Could not remove student from position %d\n",pos);
	else
	{
		printf("Removed student from position %d\n",pos);
		free(temp);
		temp=NULL;
	}
	print_student_details();

	printf("Clearing test data\r\n");
	_clear_data();
	print_student_details();
	return 0;

}

static void _add_course_test_data(void)
{

	bool result;

	printf("Adding ece sem1 courses\n");
	course_info_t *course1;
	course1=(course_info_t *)calloc(1,sizeof(course_info_t));
	course1->id=1;
	memcpy(course1->name,"CAD",3);
	result=course_push(&ece_sem1,course1);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

	course_info_t *course2;
	course2=(course_info_t *)calloc(1,sizeof(course_info_t));
	course2->id=2;
	memcpy(course2->name,"Python",6);
	result=course_push(&ece_sem1,course2);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

	course_info_t *course3;
	course3=(course_info_t *)calloc(1,sizeof(course_info_t));
	course3->id=3;
	memcpy(course3->name,"Chemistry",9);
	result=course_push_to_front(&ece_sem1,course3);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

	course_info_t *course4;
	course4=(course_info_t *)calloc(1,sizeof(course_info_t));
	course4->id=4;
	memcpy(course4->name,"CProgramming",12);
	result=course_insert(&ece_sem1,course4,2);
	if(result==true)
		printf("Course added to list successfully\r\n");
	else
		printf("Failed to add course to the list\r\n");

	/////////////////////////////////////////////////////
	printf("Adding ece sem2 courses\n");
	course_info_t *course5;
	course5=(course_info_t *)calloc(1,sizeof(course_info_t));
	course5->id=1;
	memcpy(course5->name,"Physics",7);
	result=course_push(&ece_sem2,course5);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

	course_info_t *course6;
	course6=(course_info_t *)calloc(1,sizeof(course_info_t));
	course6->id=2;
	memcpy(course6->name,"Mathematics2",12);
	result=course_push(&ece_sem2,course6);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

	course_info_t *course7;
	course7=(course_info_t *)calloc(1,sizeof(course_info_t));
	course7->id=3;
	memcpy(course7->name,"Civil",5);
	result=course_push_to_front(&ece_sem2,course7);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

	course_info_t *course8;
	course8=(course_info_t *)calloc(1,sizeof(course_info_t));
	course8->id=4;
	memcpy(course8->name,"Electrical",12);
	result=course_insert(&ece_sem2,course8,5);
	if(result==true)
		printf("Course added to list successfully\n");
	else
		printf("Failed to add course to the list\n");

}

static void _print_course_test_details(linked_list_t *list)
{
	course_init(list);
	course_info_t *temp1;
	uint16_t pos=2;
	uint16_t size;

	printf("Adding course test data\r\n");
	_add_course_test_data();
	print_course_details(list);
	size=course_get_size(*list);
	printf("The number of courses is %d\r\n",size);

	printf("Removing course from position %d\n",pos);
	temp1=course_pop_by_position(list,pos);
	if(temp1==NULL)
		printf("Could not remove course from position %d\n",pos);
	else
	{
		printf("Removed course from position %d\n",pos);
		free(temp1);
		temp1=NULL;
	}

	print_course_details(list);
	size=course_get_size(*list);
	printf("The number of courses is %d\r\n",size);



	printf("Clearing course test data\r\n");
	_clear_course_data(list);
	print_course_details(list);


}

static void _add_test_data(void)
{
	bool result;


	student_info_t* student1;
	student1 = (student_info_t*)calloc(1,sizeof (student_info_t));
	student1->roll_number=5;
	memcpy(student1->name,"John",4);
	result=students_push_to_front(student1);
	if(result==true)
	{
		printf("Successfully added student1 to the list\r\n");
	}
	else
		printf("Failed to add student1 to the list\r\n");

	student_info_t* student2;
	student2 = (student_info_t*)calloc(1,sizeof (student_info_t));
	student2->roll_number=2;
	memcpy(student2->name,"Jack",5);
	result=students_push_to_front(student2);
	if(result==true)
	{
		printf("Successfully added student2 to the list\r\n");
	}
	else
		printf("Failed to add student2 to the list\r\n");


	student_info_t* student3;
	student3 = (student_info_t*)calloc(1,sizeof (student_info_t));
	student3->roll_number=8;
	memcpy(student3->name,"Janice",6);
	result=students_push_to_front(student3);
	if(result==true)
	{
		printf("Successfully added student3 to the list\r\n");
	}
	else
		printf("Failed to add student3 to the list\r\n");

	student_info_t* new_student;
	new_student =(student_info_t*)calloc(1,sizeof(student_info_t));
	new_student->roll_number=10;
	memcpy(new_student->name,"Jane",4);
	result=insert_new_student(new_student,3);
}

static void _clear_data(void)
{
	student_info_t* temp;

	temp=students_peek();
	while(temp!=NULL)
	{
		students_pop();
		free(temp);
		temp=students_peek();
	}
}



static void _clear_course_data(linked_list_t *list)
{
	if(!list)
		return;
	course_info_t* temp;

	temp=course_peek(list);

	while(temp!=NULL)
	{
		course_pop(list);
		free(temp);
		temp=course_peek(list);
	}
}
