
#ifndef COURSES_H_
#define COURSES_H_

#define COURSE_NAME_MAX_SIZE_BYTES 100

#include "utils/linked_list.h"
#include<stdint.h>

typedef struct _course_info {
	uint16_t id;
	uint8_t name[COURSE_NAME_MAX_SIZE_BYTES];
	linked_list_item_t item;
}course_info_t;


void course_init(linked_list_t *list);
bool course_push(linked_list_t *list,course_info_t *course);
course_info_t* course_pop(linked_list_t *list);
course_info_t* course_peek(linked_list_t *list);
bool course_push_to_front(linked_list_t *list,course_info_t *course);
bool course_insert(linked_list_t *list,course_info_t *course,uint16_t position);
course_info_t* course_pop_by_position(linked_list_t *list,uint16_t position);
uint16_t course_get_size(linked_list_t list);
void print_course_details(linked_list_t *list);

//void course_init(void);
//bool course_push(course_info_t *course);
//course_info_t* course_pop(void);
//course_info_t* course_peek(void);
//uint16_t course_get_size(void);
//bool push_to_front(course_info_t *course);
//bool insert_course(course_info_t *course,uint16_t position);
//course_info_t* course_pop_by_position(uint16_t position);

#endif /* COURSES_H_ */
