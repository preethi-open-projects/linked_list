#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include<stdlib.h>
#include "courses.h"
#include "utils/linked_list.h"

//static linked_list_t _list;
static void _print_item_details(void* parent);
/////////////////////////////////////////////////Public Functions
void course_init(linked_list_t *list)
{
	linked_list_init(list);
}



bool course_push(linked_list_t *list,course_info_t *course)
{
	if(!course)
		return false;

	course->item.parent = (void *)course;

	return linked_list_push(list, &course->item);

}



bool course_push_to_front(linked_list_t *list,course_info_t *course)
{
	if(!course)
		return false;

	course->item.parent = (void *)course;

	return linked_list_push_to_front(list, &course->item);

}



course_info_t* course_pop(linked_list_t* list)
{
	linked_list_item_t* item;

	item = linked_list_pop(list);
	if(item == NULL)
		return NULL;

	return(course_info_t*)item->parent;
}



course_info_t* course_pop_by_position(linked_list_t *list,uint16_t position)
{
	linked_list_item_t* item;

	item=linked_list_pop_by_position(list, position);
	if(item==NULL)
		return NULL ;

	return(course_info_t*)item->parent;
}



bool course_insert(linked_list_t *list,course_info_t *course,uint16_t position)
{
	if(!course)
		return false;

	course->item.parent=(void *)course;

	return linked_list_insert(list, &course->item, position);

}


course_info_t* course_peek(linked_list_t *list)
{
	linked_list_item_t* item;

	item = linked_list_peek(list);
	if(item == NULL)
		return NULL;

	return(course_info_t*)item->parent;
}



uint16_t course_get_size(linked_list_t list)
{
	return list.size;
}



void print_course_details(linked_list_t *list)
{
	linked_list_print_details(list, _print_item_details);
}
///////////////////////////////////////////////////////Private functions
static void _print_item_details(void* parent)
{
	course_info_t* temp;

	if(!parent)
		return;

	temp = (course_info_t*) parent;
	printf("%4d, %s\r\n",temp->id,temp->name);
}
