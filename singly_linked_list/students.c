#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include "students.h"

static student_info_t *_head;
static student_info_t *_tail;
static uint16_t _total_students;
static student_info_t* _get_element_by_position(uint16_t);
//static student_info_t* get_tail_element(void);
//////////////////////////////////////////////////////Public functions
bool students_push(student_info_t *new_student)
{

	if(new_student==NULL)
		return false;

	if(_head == NULL)
	{
		_head = new_student;
	}
	else
	{
		_tail->next = new_student;
	}
	_tail = new_student;
	new_student->next = NULL;

	//_total_students++;
	return true;
}

bool students_push_to_front(student_info_t *new_student)
{

	if(new_student==NULL)
		return false;

	if(_head==NULL)
	{

		_head=new_student;
		_tail=new_student;
		new_student->next=NULL;

	}
	else
	{
		new_student->next=_head;
		_head = new_student;

	}

	_total_students++;

	return true;
}


student_info_t* students_pop(void)
{
	student_info_t* first;
	if(_head==NULL)
		return NULL;

	first=_head;
	_head=_head->next;
	_total_students--;
	if(_head==NULL)
		_tail = NULL;

	first->next = NULL;
	return first;

	return NULL;
}

student_info_t* students_pop_by_position(uint16_t position)
{
	student_info_t *temp;
	student_info_t *prev;

	if(position==0)
		return NULL;

	if(position==1)
		return students_pop();

	if(position > _total_students)
		return NULL;


	prev=_get_element_by_position(position-1);
	if(prev==NULL)
		return NULL;

	temp=prev->next;
	prev->next=temp->next;
	temp->next=NULL;

	if(position==_total_students)
		_tail=prev;

	return temp;

}

student_info_t* students_peek(void)
{
	return _head;
}

uint16_t students_get_total(void)
{
	return _total_students;

//	student_info_t *temp;
//	uint16_t number_of_students=0;
//	temp=_head;
//
//	while(temp!=NULL)
//	{
//		number_of_students++;
//		temp=temp->next;
//	}
//	return number_of_students;

}
uint16_t get_student_position(uint16_t roll_number)
{
	student_info_t *temp;
	temp=_head;
	uint16_t position=1;

	while(temp!=NULL)
	{
		if(temp->roll_number==roll_number)
			return position;

		temp=temp->next;
		position++;
	}


	return 0;

}

bool insert_new_student(student_info_t *new_student , uint16_t position)
{
	student_info_t *prev;


	if(new_student==NULL)
		return false;


	if(position==1)
		return students_push_to_front(new_student);

	prev=_get_element_by_position(position-1);
	if(prev==NULL)
		return students_push(new_student);

	new_student->next=prev->next;
	prev->next=new_student;
	_total_students++;

	return false;
}

void print_student_details(void)
{
	student_info_t *temp;
	uint16_t total_students;
	temp = _head;
	total_students=0;

	while(temp!=NULL)
	{
		printf("%6d,%s\r\n",temp->roll_number,temp->name);
		total_students++;
		temp=temp->next;
	}
	printf("Total Students :%d\r\n",total_students);

}
///////////////////////////////////////////////////////Private functions

static student_info_t* _get_element_by_position(uint16_t position)
{
	uint16_t pos=0;
	student_info_t *temp;


	if(position==1)
		return _head;

	temp=_head;
	pos=1;

	while(temp!=NULL)
	{
		if(pos==position)
			return temp;

		pos++;
		temp=temp->next;
	}

	return NULL;
}
//static student_info_t* get_tail_element(void)
//{
//
//	student_info_t *temp;
//
//	if(_head==NULL)
//	{
//		return NULL;
//	}
//	else
//	{
//		temp=_head;
//		while(temp->next != NULL)
//			temp=temp->next;
//
//		return temp;
//	}
//
//}
//

