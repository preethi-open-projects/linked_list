#ifndef STUDENTS_H_
#define STUDENTS_H_

#include<stdint.h>
#include<stdbool.h>

#define NAME_MAX_LEN_BYTES 200

typedef struct __student_info_ {
	uint16_t roll_number;
	uint8_t name[NAME_MAX_LEN_BYTES];
	struct __student_info_ *next;
} student_info_t;

bool students_push(student_info_t *new_student);
student_info_t* students_pop(void);
student_info_t* students_peek(void);
bool students_push_to_front(student_info_t *new_student);
uint16_t students_get_total(void);
student_info_t* students_pop_by_position(uint16_t position);
uint16_t get_student_position(uint16_t roll_number);

bool insert_new_student(student_info_t *new_student , uint16_t position);

void print_student_details(void);



#endif /* STUDENTS_H_ */
