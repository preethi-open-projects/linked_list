
#ifndef ARRAY_H_
#define ARRAY_H_

#include<stdlib.h>
#include<stdbool.h>



typedef struct __array_info{
	uint8_t *array;
	uint16_t size;
	uint16_t length;
}array_info_t;

bool array_push(array_info_t* ,uint16_t new_element);
bool array_pop(array_info_t* ,uint16_t *return_value);

//bool push_to_front(uint16_t new_element);
//bool pop_by_position(uint16_t delete_position);

void print_array(array_info_t* array);

#endif /* ARRAY_H_ */
