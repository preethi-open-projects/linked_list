#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<stdint.h>
#include"array.h"



int main(void)
{
	array_info_t *array;
	array->size=5;
	uint8_t arr[]={1,2,3,4};
	array->length=4;
	uint16_t return_value=1;

	bool result;
	result=array_push(arr, 6);

	if(result==true)
		print_array(arr);

	bool result1;
	result1=array_pop(arr, &return_value);
	if(result1==true)
		print_array(arr);


	return 0;
}
