#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>

#define NUMBER_OF_ELEMENTS 5

bool add_new_element(uint16_t *array,uint16_t size,uint16_t insert_position,uint16_t new_element);

int main(void)
{
	uint16_t array[NUMBER_OF_ELEMENTS]={1,2,3,5,6};
	uint16_t length = (sizeof(array)/sizeof(array[0]));
	uint16_t insert_index= 9;
	uint16_t new_element = 4,index,return_value;

	
	printf("The original array is :");
	for(index=0;index<length;index++)
		printf("%d ",array[index]);
	
	printf("\n");

	return_value=add_new_element(array,length,insert_index,new_element);
	
	if(return_value==1)
	{
	printf("New array is :");
	for(index=0;index<length;index++)
		printf("%d ",array[index]);

	printf("\n");
	}
	
	else
		printf("The function has been crashed.\n");

	return 0;
}

bool add_new_element(uint16_t *array,uint16_t size,uint16_t insert_position,uint16_t new_element)
{
	if(array==NULL)
		return false;
	
	else if(insert_position>size)
		return false;
	
	else if(size==0)
		return false;

	uint16_t index;

	for(index=size-1;index>insert_position;index--)
	{
		array[index]=array[index-1];
	//	printf("%d\r\n",index);
	}
	array[insert_position]=new_element;
	
	return true;

}

