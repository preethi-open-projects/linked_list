#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>

#define NUMBER_OF_ELEMENTS 5

bool delete_element(uint16_t *array,uint16_t size,uint16_t delete_position);

int main(void)
{
	uint16_t array[NUMBER_OF_ELEMENTS]={1,2,6,3,4};
	uint16_t delete_index=3;
	uint16_t array_size=(sizeof(array)/sizeof(array[0]));
	uint16_t index;

	printf("The original array is :");
	for(index=0;index<array_size;index++)
		printf("%d ",array[index]);

	printf("\n");
	
	printf("The element deleted is at index %d in original array\n",delete_index);
	delete_element(array,array_size,delete_index);

	printf("The new array is :");
	for(index=0;index<array_size-1;index++)
		printf("%d ",array[index]);

	printf("\n");

	return 0;
}

bool delete_element(uint16_t *array,uint16_t size, uint16_t delete_position)
{
	if(array==NULL)
		return false;
	
	else if(delete_position>size)
		return false;
	
	else if(size==0)
		return false;

	uint16_t index;

	for(index=delete_position;index<size-1;index++)
		array[index]=array[index+1];

	return true;
}
